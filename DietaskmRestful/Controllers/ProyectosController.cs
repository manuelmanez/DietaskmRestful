﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DietaskmRestfulData;

namespace DietaskmRestful.Controllers
{
    [RoutePrefix("api/proyectos")]
    public class ProyectosController : ApiController
    {
        DietaskmEntities repo = new DietaskmEntities();

        /* 
         * Obtener todos los proyectos
         * GET - api/proyectos
         */
        [Route("")]
        public HttpResponseMessage Get()
        {
            HttpResponseMessage message;

            message = Request.CreateResponse(HttpStatusCode.OK, repo.Proyectos.ToList());
            return message;
        }

        /* 
         * Obtener un proyecto por Id
         * GET - api/proyectos/1
         */
        [Route("{id:int:min(1)}", Name = "GetProyectoById")]
        public HttpResponseMessage Get(int id)
        {
            Proyecto proyecto;
            HttpResponseMessage message;

            proyecto = repo.Proyectos.FirstOrDefault(p => p.IdProyecto == id);

            if(proyecto != null)
            {
                message = Request.CreateResponse(HttpStatusCode.OK, proyecto);
            }
            else
            {
                message = Request.CreateResponse(HttpStatusCode.NotFound, "El proyecto " + id.ToString() + " no existe");
            }

            return message;
        }

        /* 
         * Obtener proyectos abiertos
         * GET - api/proyectos/abiertos
         */
        [Route("abiertos")]
        public HttpResponseMessage GetProyectosAbiertos()
        {
            HttpResponseMessage message;

            message = Request.CreateResponse(HttpStatusCode.OK, repo.Proyectos.Where(p => p.Cerrado == false).ToList());

            return message;
        }

        /* 
         * Crear Proyecto
         * POST api/proyectos - proyecto en body
         */
        [Route("")]
        public HttpResponseMessage Post([FromBody] Proyecto proyecto)
        {
            HttpResponseMessage message;

            try
            {
                repo.Proyectos.Add(proyecto);
                repo.SaveChanges();

                message = Request.CreateResponse(HttpStatusCode.Created, proyecto);
                message.Headers.Location = new Uri(Url.Link("GetProyectoById", new { id = proyecto.IdProyecto}));
                return message;
            }
            catch (Exception ex)
            {
                message = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

            return message;
            
        }

        /* 
         * Elimina un proyecto
         * DELETE - api/proyectos/1
         */
        [Route("{id:int:min(1)}")]
        public HttpResponseMessage Delete(int id)
        {
            Proyecto proyecto;
            HttpResponseMessage message;

            try
            {
                proyecto = repo.Proyectos.FirstOrDefault(p => p.IdProyecto == id);

                if (proyecto != null)
                {
                    repo.Proyectos.Remove(proyecto);
                    repo.SaveChanges();
                    message = Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    message = Request.CreateErrorResponse(HttpStatusCode.NotFound, "Proyecto " + id.ToString() + " no existe");
                }
            }
            catch(Exception ex)
            {
                message = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

            return message;
            
        }

        /* 
         * Modifica un proyecto
         * PUT - api/proyectos/1 - proyecto en body
         */
        [Route("{id:int:min(1)}")]
        public HttpResponseMessage Put(int id, [FromBody] Proyecto proyecto)
        {
            Proyecto proyectoOriginal;
            HttpResponseMessage message;

            try
            {
                proyectoOriginal = repo.Proyectos.FirstOrDefault(p => p.IdProyecto == id);

                if (proyectoOriginal != null)
                {
                    proyectoOriginal.Nombre = proyecto.Nombre;
                    proyectoOriginal.Descripcion = proyecto.Descripcion;
                    proyectoOriginal.Cerrado = proyecto.Cerrado;

                    repo.SaveChanges();

                    message = Request.CreateResponse(HttpStatusCode.OK, proyectoOriginal);
                }
                else
                {
                    message = Request.CreateErrorResponse(HttpStatusCode.NotFound, "El proyecto " + id.ToString() + " no existe");
                }
            }
            catch (Exception ex)
            {
                message = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

            return message;
        }

    }
}
