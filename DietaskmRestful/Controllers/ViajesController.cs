﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DietaskmRestfulData;

namespace DietaskmRestful.Controllers
{
    [RoutePrefix("api/proyectos/{idProyecto:int:min(1)}/viajes")]
    public class ViajesController : ApiController
    {
        DietaskmEntities repo = new DietaskmEntities();

        /* 
         * Obtener todos los viajes de un proyecto
         * GET - api/proyectos/1/viajes
         */
        [Route("")]
        public HttpResponseMessage Get(int idProyecto)
        {
            HttpResponseMessage message;

            message = Request.CreateResponse(HttpStatusCode.OK, repo.Viajes.Where(v => v.IdProyecto == idProyecto).ToList());
            return message;
        }

        /* 
         * Obtener un viaje por Id
         * GET - api/proyectos/1/viajes/1
         */
        [Route("{idViaje:int:min(1)}", Name = "GetViajeById")]
        public HttpResponseMessage Get(int idProyecto, int idViaje)
        {
            Viaje viaje;
            HttpResponseMessage message;

            viaje = repo.Viajes.FirstOrDefault(v => v.IdViaje == idViaje);


            if (viaje != null)
            {
                if (viaje.IdProyecto == idProyecto)
                {
                    message = Request.CreateResponse(HttpStatusCode.OK, viaje);
                }
                else
                {
                    message = Request.CreateResponse(HttpStatusCode.BadRequest,
                        "El viaje " + idViaje.ToString() + " no pertenece al proyecto " + idProyecto);
                }
            }
            else
            {
                message = Request.CreateResponse(HttpStatusCode.NotFound, "El viaje " + idViaje.ToString() + " no existe");
            }

            return message;
        }

        /* 
         * Crear Viaje
         * POST api/proyectos/1/viaje - viaje en body
         */
        [Route("")]
        public HttpResponseMessage Post(int idProyecto, [FromBody] Viaje viaje)
        {
            HttpResponseMessage message;

            try
            {
                viaje.IdProyecto = idProyecto;
                repo.Viajes.Add(viaje);
                repo.SaveChanges();

                message = Request.CreateResponse(HttpStatusCode.Created, viaje);
                message.Headers.Location = new Uri(Url.Link("GetViajeById", new { idViaje = viaje.IdViaje }));
                return message;
            }
            catch (Exception ex)
            {
                message = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

            return message;

        }


        /* 
         * Elimina un viaje
         * DELETE - api/proyectos/1/viajes/1
         */
        [Route("{idViaje:int:min(1)}")]
        public HttpResponseMessage Delete(int idProyecto, int idViaje)
        {
            Viaje viaje;
            HttpResponseMessage message;

            viaje = repo.Viajes.FirstOrDefault(v => v.IdViaje == idViaje);

            try
            {
                if (viaje != null)
                {
                    if (viaje.IdProyecto == idProyecto)
                    {
                        repo.Viajes.Remove(viaje);
                        repo.SaveChanges();
                        message = Request.CreateResponse(HttpStatusCode.OK);
                    }
                    else
                    {
                        message = Request.CreateResponse(HttpStatusCode.BadRequest,
                            "El viaje " + idViaje.ToString() + " no pertenece al proyecto " + idProyecto);
                    }
                }
                else
                {
                    message = Request.CreateResponse(HttpStatusCode.NotFound, "El viaje " + idViaje.ToString() + " no existe");
                }
            }
            catch (Exception ex)
            {
                message = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

            return message;
        }

        /* 
         * Modifica un proyecto
         * PUT - api/proyectos/1/viajes/1 - viaje en body
         */
        [Route("{idViaje:int:min(1)}")]
        public HttpResponseMessage Put(int idProyecto, int idViaje, [FromBody] Viaje viaje)
        {
            Viaje viajeOriginal;
            HttpResponseMessage message;

            viajeOriginal = repo.Viajes.FirstOrDefault(v => v.IdViaje == idViaje);

            try
            {
                if (viajeOriginal != null)
                {
                    if (viajeOriginal.IdProyecto == idProyecto)
                    {
                        viajeOriginal.Motivo = viaje.Motivo;
                        viajeOriginal.IdProyecto = viaje.IdProyecto;

                        repo.SaveChanges();

                        message = Request.CreateResponse(HttpStatusCode.OK, viajeOriginal);
                    }
                    else
                    {
                        message = Request.CreateResponse(HttpStatusCode.BadRequest,
                            "El viaje " + idViaje.ToString() + " no pertenece al proyecto " + idProyecto);
                    }
                }
                else
                {
                    message = Request.CreateResponse(HttpStatusCode.NotFound, "El viaje " + idViaje.ToString() + " no existe");
                }
            }
            catch (Exception ex)
            {
                message = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

            return message;
        }

    }
}
