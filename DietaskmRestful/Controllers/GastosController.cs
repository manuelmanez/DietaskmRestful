﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DietaskmRestfulData;
using Newtonsoft.Json;

namespace DietaskmRestful.Controllers
{
    [RoutePrefix("api/empleados/{idEmpleado:int:min(1)}/gastos")]
    public class GastosController : ApiController
    {
        DietaskmEntities repo = new DietaskmEntities();

        /* 
         * Obtener todos los gastos de un empleado
         * GET - api/empleados/1/gastos
         */
        [Route("")]
        public HttpResponseMessage Get(int idEmpleado)
        {
            HttpResponseMessage message;

            message = Request.CreateResponse(HttpStatusCode.OK, repo.Gastos.Where(g => g.IdEmpleado == idEmpleado).ToList());
            return message;
        }

        /* 
         * Obtener un gasto por Id
         * GET - api/empleados/1/gastos/1
         */
        [Route("{idGasto:int:min(1)}", Name = "GetGastoById")]
        public HttpResponseMessage Get(int idEmpleado, int idGasto)
        {
            Gasto gasto;
            HttpResponseMessage message;

            gasto = repo.Gastos.FirstOrDefault(g => g.IdGasto == idGasto);


            if (gasto != null)
            {
                if (gasto.IdEmpleado == idEmpleado)
                {
                    message = Request.CreateResponse(HttpStatusCode.OK, gasto);
                }
                else
                {
                    message = Request.CreateResponse(HttpStatusCode.BadRequest,
                        "El gasto " + idGasto.ToString() + " no pertenece al empleado " + idEmpleado);
                }
            }
            else
            {
                message = Request.CreateResponse(HttpStatusCode.NotFound, "El gasto " + idGasto.ToString() + " no existe");
            }

            return message;
        }

        /* 
         * Obtener gastos pendientes e imputados (aquellos todavía modificables)
         * GET - api/empleados/1/gastos/modificables
         */
        [Route("modificables")]
        public HttpResponseMessage GetGastosModificables(int idEmpleado)
        {
            HttpResponseMessage message;

            message = Request.CreateResponse(HttpStatusCode.OK, repo.Gastos.Where(g => g.IdEmpleado == idEmpleado).Where(g =>g.Estado < 2).ToList());
            return message;
        }

        /* 
         * Crear Gasto
         * POST api/empleados/1/gastos - gasto en body
         */
        [Route("")]
        public HttpResponseMessage Post(int idEmpleado, [FromBody] Gasto gasto)
        {
            HttpResponseMessage message;

            try
            {
                gasto.IdEmpleado = idEmpleado;
                repo.Gastos.Add(gasto);
                repo.SaveChanges();

                message = Request.CreateResponse(HttpStatusCode.Created, gasto);
                message.Headers.Location = new Uri(Url.Link("GetGastoById", new { idGasto = gasto.IdGasto }));
                return message;
            }
            catch (Exception ex)
            {
                message = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

            return message;

        }

        /* 
         * Elimina un gasto si es modificable (no está aprobado todavía: estado < 2)
         * DELETE - api/empleado/1/gastos/1
         */
        [Route("{idGasto:int:min(1)}")]
        public HttpResponseMessage Delete(int idEmpleado, int idGasto)
        {
            Gasto gasto;
            HttpResponseMessage message;

            gasto = repo.Gastos.FirstOrDefault(g => g.IdGasto == idGasto);

            try
            {
                if (gasto != null)
                {
                    if (gasto.IdEmpleado == idEmpleado)
                    {
                        if (gasto.Estado < 2)
                        {
                            repo.Gastos.Remove(gasto);
                            repo.SaveChanges();
                            message = Request.CreateResponse(HttpStatusCode.OK);
                        }
                        else
                        {
                            message = Request.CreateResponse(HttpStatusCode.Forbidden,
                            "El gasto " + idGasto.ToString() + " no puede eliminarse. Su estado es " + gasto.Estado);
                        }
                    }
                    else
                    {
                        message = Request.CreateResponse(HttpStatusCode.BadRequest,
                            "El gasto " + idGasto.ToString() + " no pertenece al empleado " + idEmpleado);
                    }
                }
                else
                {
                    message = Request.CreateResponse(HttpStatusCode.NotFound, "El gasto " + idGasto.ToString() + " no existe");
                }
            }
            catch (Exception ex)
            {
                message = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

            return message;
        }

        /* 
         * Modifica un gasto si es modificable (no está aprobado todavía: estado < 2)
         * PUT - api/empleados/1/gastos/1 - gasto en body
         */
        [Route("{idGasto:int:min(1)}")]
        public HttpResponseMessage Put(int idEmpleado, int idGasto, [FromBody] Gasto gasto)
        {
            Gasto gastoOriginal;
            HttpResponseMessage message;

            gastoOriginal = repo.Gastos.FirstOrDefault(g => g.IdGasto == idGasto);

            try
            {
                if (gastoOriginal != null)
                {
                    if (gastoOriginal.IdEmpleado == idEmpleado)
                    {
                        if (gastoOriginal.Estado < 2) {
                            gastoOriginal.IdViaje = gasto.IdViaje;
                            gastoOriginal.Fecha = gasto.Fecha;
                            gastoOriginal.Tipo = gasto.Tipo;
                            gastoOriginal.Descripcion = gasto.Descripcion;
                            gastoOriginal.Coste = gasto.Coste;
                            gastoOriginal.Adjunto = gasto.Adjunto;

                            repo.SaveChanges();

                            message = Request.CreateResponse(HttpStatusCode.OK, gastoOriginal);
                        }
                        else
                        {
                            message = Request.CreateResponse(HttpStatusCode.Forbidden,
                            "El gasto " + idGasto.ToString() + " no puede modificarse. Su estado es " + gasto.Estado);
                        }
                    }
                    else
                    {
                        message = Request.CreateResponse(HttpStatusCode.BadRequest,
                            "El gasto " + idGasto.ToString() + " no pertenece al empleado " + idEmpleado);
                    }
                }
                else
                {
                    message = Request.CreateResponse(HttpStatusCode.NotFound, "El gasto " + idGasto.ToString() + " no existe");
                }
            }
            catch (Exception ex)
            {
                message = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

            return message;
        }

        /* 
         * Modifica el estado de un gasto la siguiente
         * POST - api/empleados/1/gastos/1
         */
        [Route("{idGasto:int:min(1)}/mover")]
        public HttpResponseMessage Post(int idEmpleado, int idGasto)
        {
            Gasto gasto;
            HttpResponseMessage message;

            gasto = repo.Gastos.FirstOrDefault(g => g.IdGasto == idGasto);

            try
            {
                if (gasto != null)
                {
                    if (gasto.IdEmpleado == idEmpleado)
                    {
                        if (gasto.Estado < 3)
                        {
                            gasto.Estado = gasto.Estado + 1;

                            repo.SaveChanges();

                            message = Request.CreateResponse(HttpStatusCode.OK, gasto);
                        }
                        else
                        {
                            message = Request.CreateResponse(HttpStatusCode.Forbidden,
                            "El gasto " + idGasto.ToString() + "ya se ha pagado");
                        }
                    }
                    else
                    {
                        message = Request.CreateResponse(HttpStatusCode.BadRequest,
                            "El gasto " + idGasto.ToString() + " no pertenece al empleado " + idEmpleado);
                    }
                }
                else
                {
                    message = Request.CreateResponse(HttpStatusCode.NotFound, "El gasto " + idGasto.ToString() + " no existe");
                }
            }
            catch (Exception ex)
            {
                message = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

            return message;
        }

    }
}
