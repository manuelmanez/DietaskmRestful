﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DietaskmRestfulData;

namespace DietaskmRestful.Controllers
{
    [RoutePrefix("api/empleados")]
    public class EmpleadosController : ApiController
    {
        DietaskmEntities repo = new DietaskmEntities();

        /* 
         * Obtener todos los empleados
         * GET - api/empleados
         */
        [Route("")]
        public HttpResponseMessage Get()
        {
            HttpResponseMessage message;

            message = Request.CreateResponse(HttpStatusCode.OK, repo.Empleados.ToList());
            return message;
        }

        /* 
         * Obtener un empleado por Id
         * GET - api/empleados/1
         */
        [Route("{id:int:min(1)}", Name = "GetEmpleadoById")]
        public HttpResponseMessage Get(int id)
        {
            Empleado empleado;
            HttpResponseMessage message;

            empleado = repo.Empleados.FirstOrDefault(p => p.IdEmpleado == id);

            if (empleado != null)
            {
                message = Request.CreateResponse(HttpStatusCode.OK, empleado);
            }
            else
            {
                message = Request.CreateResponse(HttpStatusCode.NotFound, "El empleado " + id.ToString() + " no existe");
            }

            return message;
        }

        /* 
         * Crear Empleado
         * POST api/empleados - empleado en body
         */
        [Route("")]
        public HttpResponseMessage Post([FromBody] Empleado empleado)
        {
            HttpResponseMessage message;

            try
            {
                repo.Empleados.Add(empleado);
                repo.SaveChanges();

                message = Request.CreateResponse(HttpStatusCode.Created, empleado);
                message.Headers.Location = new Uri(Url.Link("GetEmpleadoById", new { id = empleado.IdEmpleado }));
                return message;
            }
            catch (Exception ex)
            {
                message = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

            return message;

        }

        /* 
         * Elimina un empleado
         * DELETE - api/empleados/1
         */
        [Route("{id:int:min(1)}")]
        public HttpResponseMessage Delete(int id)
        {
            Empleado empleado;
            HttpResponseMessage message;

            try
            {
                empleado = repo.Empleados.FirstOrDefault(p => p.IdEmpleado == id);

                if (empleado != null)
                {
                    repo.Empleados.Remove(empleado);
                    repo.SaveChanges();
                    message = Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    message = Request.CreateErrorResponse(HttpStatusCode.NotFound, "Empleado " + id.ToString() + " no existe");
                }
            }
            catch (Exception ex)
            {
                message = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

            return message;

        }

        /* 
         * Modifica un empleado
         * PUT - api/empleados/1 - empleado en body
         */
        [Route("{id:int:min(1)}")]
        public HttpResponseMessage Put(int id, [FromBody] Empleado empleado)
        {
            Empleado empleadoOriginal;
            HttpResponseMessage message;

            try
            {
               empleadoOriginal = repo.Empleados.FirstOrDefault(p => p.IdEmpleado == id);

                if (empleadoOriginal != null)
                {
                    empleadoOriginal.Nombre = empleado.Nombre;
                    empleadoOriginal.Apellidos = empleado.Apellidos;
                    empleadoOriginal.Telefono = empleado.Telefono;
                    empleadoOriginal.Email = empleado.Email;

                    repo.SaveChanges();

                    message = Request.CreateResponse(HttpStatusCode.OK, empleadoOriginal);
                }
                else
                {
                    message = Request.CreateErrorResponse(HttpStatusCode.NotFound, "El empleado " + id.ToString() + " no existe");
                }
            }
            catch (Exception ex)
            {
                message = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

            return message;
        }

    }
}
